/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/app/app.controller.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.AppController = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const app_service_1 = __webpack_require__("./src/app/app.service.ts");
let AppController = class AppController {
    constructor(appService) {
        this.appService = appService;
    }
    getData() {
        return this.appService.getData();
    }
};
tslib_1.__decorate([
    (0, common_1.Get)(),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], AppController.prototype, "getData", null);
AppController = tslib_1.__decorate([
    (0, common_1.Controller)(),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof app_service_1.AppService !== "undefined" && app_service_1.AppService) === "function" ? _a : Object])
], AppController);
exports.AppController = AppController;


/***/ }),

/***/ "./src/app/app.module.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.AppModule = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const app_controller_1 = __webpack_require__("./src/app/app.controller.ts");
const app_service_1 = __webpack_require__("./src/app/app.service.ts");
const category_module_1 = __webpack_require__("./src/category/category.module.ts");
const picture_module_1 = __webpack_require__("./src/picture/picture.module.ts");
const product_module_1 = __webpack_require__("./src/product/product.module.ts");
let AppModule = class AppModule {
};
AppModule = tslib_1.__decorate([
    (0, common_1.Module)({
        imports: [category_module_1.CategoryModule, picture_module_1.PictureModule, product_module_1.ProductModule],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService],
    })
], AppModule);
exports.AppModule = AppModule;


/***/ }),

/***/ "./src/app/app.service.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.AppService = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
let AppService = class AppService {
    getData() {
        return { message: 'Welcome to product-service!' };
    }
};
AppService = tslib_1.__decorate([
    (0, common_1.Injectable)()
], AppService);
exports.AppService = AppService;


/***/ }),

/***/ "./src/category/category.controller.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a, _b, _c;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.CategoryController = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const category_service_1 = __webpack_require__("./src/category/category.service.ts");
const _dto_1 = __webpack_require__("../../libs/dto/src/index.ts");
const swagger_1 = __webpack_require__("@nestjs/swagger");
let CategoryController = class CategoryController {
    constructor(categoryService) {
        this.categoryService = categoryService;
    }
    create(data) {
        return this.categoryService.create(data);
    }
    findAll() {
        return this.categoryService.findAll();
    }
    findOne(id) {
        return this.categoryService.findOne(id);
    }
    update(id, data) {
        return this.categoryService.update(id, data);
    }
    remove(id) {
        return this.categoryService.remove(id);
    }
};
tslib_1.__decorate([
    (0, common_1.Post)(),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [typeof (_b = typeof _dto_1.CategoryDto !== "undefined" && _dto_1.CategoryDto) === "function" ? _b : Object]),
    tslib_1.__metadata("design:returntype", void 0)
], CategoryController.prototype, "create", null);
tslib_1.__decorate([
    (0, common_1.Get)(),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], CategoryController.prototype, "findAll", null);
tslib_1.__decorate([
    (0, common_1.Get)(':id'),
    tslib_1.__param(0, (0, common_1.Param)('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", void 0)
], CategoryController.prototype, "findOne", null);
tslib_1.__decorate([
    (0, common_1.Patch)(':id'),
    tslib_1.__param(0, (0, common_1.Param)('id')),
    tslib_1.__param(1, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, typeof (_c = typeof _dto_1.CategoryDto !== "undefined" && _dto_1.CategoryDto) === "function" ? _c : Object]),
    tslib_1.__metadata("design:returntype", void 0)
], CategoryController.prototype, "update", null);
tslib_1.__decorate([
    (0, common_1.Delete)(':id'),
    tslib_1.__param(0, (0, common_1.Param)('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", void 0)
], CategoryController.prototype, "remove", null);
CategoryController = tslib_1.__decorate([
    (0, common_1.Controller)('category'),
    (0, swagger_1.ApiTags)('category'),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof category_service_1.CategoryService !== "undefined" && category_service_1.CategoryService) === "function" ? _a : Object])
], CategoryController);
exports.CategoryController = CategoryController;


/***/ }),

/***/ "./src/category/category.module.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.CategoryModule = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const category_service_1 = __webpack_require__("./src/category/category.service.ts");
const category_controller_1 = __webpack_require__("./src/category/category.controller.ts");
const _db_1 = __webpack_require__("../../libs/db/src/index.ts");
const config_1 = __webpack_require__("@nestjs/config");
let CategoryModule = class CategoryModule {
};
CategoryModule = tslib_1.__decorate([
    (0, common_1.Module)({
        imports: [_db_1.DbModule, config_1.ConfigModule],
        controllers: [category_controller_1.CategoryController],
        providers: [category_service_1.CategoryService, _db_1.DatabaseService]
    })
], CategoryModule);
exports.CategoryModule = CategoryModule;


/***/ }),

/***/ "./src/category/category.service.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.CategoryService = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const _db_1 = __webpack_require__("../../libs/db/src/index.ts");
let CategoryService = class CategoryService {
    constructor(dbService) {
        this.dbService = dbService;
    }
    create(data) {
        console.log(`Creating category record ${JSON.stringify(data)}...`);
        return this.dbService.category.create({
            data
        });
    }
    findAll() {
        console.log('Fetching all category records...');
        return this.dbService.category.findMany();
    }
    findOne(id) {
        console.log(`Fetching category record with id: ${id}...`);
        return this.dbService.category.findUniqueOrThrow({
            where: {
                id
            }
        });
    }
    update(id, data) {
        console.log(`Updating category record ${JSON.stringify(data)}...`);
        return this.dbService.category.update({
            where: {
                id
            },
            data
        });
    }
    remove(id) {
        console.log(`Deleting category record ${id}...`);
        return this.dbService.category.delete({
            where: {
                id
            }
        });
    }
};
CategoryService = tslib_1.__decorate([
    (0, common_1.Injectable)(),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof _db_1.DatabaseService !== "undefined" && _db_1.DatabaseService) === "function" ? _a : Object])
], CategoryService);
exports.CategoryService = CategoryService;


/***/ }),

/***/ "./src/picture/picture.controller.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a, _b, _c;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.PictureController = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const picture_service_1 = __webpack_require__("./src/picture/picture.service.ts");
const _dto_1 = __webpack_require__("../../libs/dto/src/index.ts");
const swagger_1 = __webpack_require__("@nestjs/swagger");
let PictureController = class PictureController {
    constructor(pictureService) {
        this.pictureService = pictureService;
    }
    create(data) {
        return this.pictureService.create(data);
    }
    findAll() {
        return this.pictureService.findAll();
    }
    findOne(id) {
        return this.pictureService.findOne(id);
    }
    update(id, data) {
        return this.pictureService.update(id, data);
    }
    remove(id) {
        return this.pictureService.remove(id);
    }
};
tslib_1.__decorate([
    (0, common_1.Post)(),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [typeof (_b = typeof _dto_1.PictureDto !== "undefined" && _dto_1.PictureDto) === "function" ? _b : Object]),
    tslib_1.__metadata("design:returntype", void 0)
], PictureController.prototype, "create", null);
tslib_1.__decorate([
    (0, common_1.Get)(),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], PictureController.prototype, "findAll", null);
tslib_1.__decorate([
    (0, common_1.Get)(':id'),
    tslib_1.__param(0, (0, common_1.Param)('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", void 0)
], PictureController.prototype, "findOne", null);
tslib_1.__decorate([
    (0, common_1.Patch)(':id'),
    tslib_1.__param(0, (0, common_1.Param)('id')),
    tslib_1.__param(1, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, typeof (_c = typeof _dto_1.PictureDto !== "undefined" && _dto_1.PictureDto) === "function" ? _c : Object]),
    tslib_1.__metadata("design:returntype", void 0)
], PictureController.prototype, "update", null);
tslib_1.__decorate([
    (0, common_1.Delete)(':id'),
    tslib_1.__param(0, (0, common_1.Param)('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", void 0)
], PictureController.prototype, "remove", null);
PictureController = tslib_1.__decorate([
    (0, common_1.Controller)('picture'),
    (0, swagger_1.ApiTags)('picture'),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof picture_service_1.PictureService !== "undefined" && picture_service_1.PictureService) === "function" ? _a : Object])
], PictureController);
exports.PictureController = PictureController;


/***/ }),

/***/ "./src/picture/picture.module.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.PictureModule = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const picture_service_1 = __webpack_require__("./src/picture/picture.service.ts");
const picture_controller_1 = __webpack_require__("./src/picture/picture.controller.ts");
const _db_1 = __webpack_require__("../../libs/db/src/index.ts");
const config_1 = __webpack_require__("@nestjs/config");
let PictureModule = class PictureModule {
};
PictureModule = tslib_1.__decorate([
    (0, common_1.Module)({
        imports: [_db_1.DbModule, config_1.ConfigModule],
        controllers: [picture_controller_1.PictureController],
        providers: [picture_service_1.PictureService, _db_1.DatabaseService]
    })
], PictureModule);
exports.PictureModule = PictureModule;


/***/ }),

/***/ "./src/picture/picture.service.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.PictureService = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const _db_1 = __webpack_require__("../../libs/db/src/index.ts");
let PictureService = class PictureService {
    constructor(dbService) {
        this.dbService = dbService;
    }
    create(data) {
        console.log(`Creating picture record ${JSON.stringify(data)}...`);
        return this.dbService.picture.create({
            data
        });
    }
    findAll() {
        console.log('Fetching all picture records...');
        return this.dbService.picture.findMany();
    }
    findOne(id) {
        console.log(`Fetching picture record with id: ${id}...`);
        return this.dbService.picture.findUniqueOrThrow({
            where: {
                id
            }
        });
    }
    update(id, data) {
        console.log(`Updating picture record ${JSON.stringify(data)}...`);
        return this.dbService.picture.update({
            where: {
                id
            },
            data
        });
    }
    remove(id) {
        console.log(`Deleting picture record ${id}...`);
        return this.dbService.picture.delete({
            where: {
                id
            }
        });
    }
};
PictureService = tslib_1.__decorate([
    (0, common_1.Injectable)(),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof _db_1.DatabaseService !== "undefined" && _db_1.DatabaseService) === "function" ? _a : Object])
], PictureService);
exports.PictureService = PictureService;


/***/ }),

/***/ "./src/product/product.controller.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a, _b, _c;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ProductController = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const product_service_1 = __webpack_require__("./src/product/product.service.ts");
const swagger_1 = __webpack_require__("@nestjs/swagger");
const _dto_1 = __webpack_require__("../../libs/dto/src/index.ts");
const client_1 = __webpack_require__("@prisma/client");
let ProductController = class ProductController {
    constructor(productService) {
        this.productService = productService;
    }
    create(data) {
        return this.productService.create(data);
    }
    findAll() {
        return this.productService.findAll();
    }
    findOne(id) {
        return this.productService.findOne(id);
    }
    update(id, data) {
        return this.productService.update(id, data);
    }
    remove(id) {
        return this.productService.remove(id);
    }
};
tslib_1.__decorate([
    (0, common_1.Post)(),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [typeof (_b = typeof _dto_1.ProductDto !== "undefined" && _dto_1.ProductDto) === "function" ? _b : Object]),
    tslib_1.__metadata("design:returntype", void 0)
], ProductController.prototype, "create", null);
tslib_1.__decorate([
    (0, common_1.Get)(),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], ProductController.prototype, "findAll", null);
tslib_1.__decorate([
    (0, common_1.Get)(':id'),
    tslib_1.__param(0, (0, common_1.Param)('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", void 0)
], ProductController.prototype, "findOne", null);
tslib_1.__decorate([
    (0, common_1.Patch)(':id'),
    tslib_1.__param(0, (0, common_1.Param)('id')),
    tslib_1.__param(1, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, typeof (_c = typeof client_1.Prisma !== "undefined" && client_1.Prisma.ProductUpdateInput) === "function" ? _c : Object]),
    tslib_1.__metadata("design:returntype", void 0)
], ProductController.prototype, "update", null);
tslib_1.__decorate([
    (0, common_1.Delete)(':id'),
    tslib_1.__param(0, (0, common_1.Param)('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", void 0)
], ProductController.prototype, "remove", null);
ProductController = tslib_1.__decorate([
    (0, common_1.Controller)('product'),
    (0, swagger_1.ApiTags)('product'),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof product_service_1.ProductService !== "undefined" && product_service_1.ProductService) === "function" ? _a : Object])
], ProductController);
exports.ProductController = ProductController;


/***/ }),

/***/ "./src/product/product.module.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ProductModule = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const product_service_1 = __webpack_require__("./src/product/product.service.ts");
const product_controller_1 = __webpack_require__("./src/product/product.controller.ts");
const _db_1 = __webpack_require__("../../libs/db/src/index.ts");
const config_1 = __webpack_require__("@nestjs/config");
let ProductModule = class ProductModule {
};
ProductModule = tslib_1.__decorate([
    (0, common_1.Module)({
        imports: [_db_1.DbModule, config_1.ConfigModule],
        controllers: [product_controller_1.ProductController],
        providers: [product_service_1.ProductService, _db_1.DatabaseService]
    })
], ProductModule);
exports.ProductModule = ProductModule;


/***/ }),

/***/ "./src/product/product.service.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ProductService = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const _db_1 = __webpack_require__("../../libs/db/src/index.ts");
let ProductService = class ProductService {
    constructor(dbService) {
        this.dbService = dbService;
    }
    create(data) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const request = Object.assign({}, data);
            delete request.pictures;
            let response = null;
            const existingRecord = yield this.dbService.product.findFirst({
                where: {
                    name: data.name,
                },
            });
            if (existingRecord != null) {
                throw new common_1.BadRequestException(`Duplicate Product Record found for name ${data.name}`);
            }
            try {
                response = yield this.dbService.product.create({
                    data: request
                });
                for (const picture of data.pictures) {
                    yield this.dbService.picture.create({
                        data: {
                            url: picture,
                            product: {
                                connect: {
                                    id: response.id
                                }
                            }
                        }
                    });
                }
            }
            catch (err) {
                console.log(err);
            }
            console.log(`Creating product record ${JSON.stringify(request)}...`);
            return response;
        });
    }
    findAll() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            console.log('Fetching all product records...');
            const records = yield this.dbService.product.findMany();
            for (const record of records) {
                const category = yield this.dbService.category.findUniqueOrThrow({
                    where: {
                        id: record.categoryId,
                    },
                });
                const pictures = yield this.dbService.picture.findMany({
                    where: {
                        productId: record.id,
                    },
                });
                record['categoryName'] = category.name;
                record['pictures'] = pictures;
            }
            return records;
        });
    }
    findOne(id) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            console.log('Fetching all product records...');
            const record = yield this.dbService.product.findUniqueOrThrow({
                where: {
                    id
                }
            });
            const category = yield this.dbService.category.findUniqueOrThrow({
                where: {
                    id: record.categoryId,
                },
            });
            const pictures = yield this.dbService.picture.findMany({
                where: {
                    productId: record.id,
                },
            });
            return Object.assign(Object.assign({}, record), { categoryName: category.name, pictures });
        });
    }
    update(id, data) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            console.log(`Updating product record ${JSON.stringify(data)}...`);
            return this.dbService.product.update({
                where: {
                    id
                },
                data
            });
        });
    }
    remove(id) {
        console.log(`Deleting product record ${id}...`);
        return this.dbService.product.delete({
            where: {
                id
            }
        });
    }
};
ProductService = tslib_1.__decorate([
    (0, common_1.Injectable)(),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof _db_1.DatabaseService !== "undefined" && _db_1.DatabaseService) === "function" ? _a : Object])
], ProductService);
exports.ProductService = ProductService;


/***/ }),

/***/ "../../libs/db/src/index.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
const tslib_1 = __webpack_require__("tslib");
tslib_1.__exportStar(__webpack_require__("../../libs/db/src/lib/db.module.ts"), exports);
tslib_1.__exportStar(__webpack_require__("../../libs/db/src/lib/db.service.ts"), exports);


/***/ }),

/***/ "../../libs/db/src/lib/db.module.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.DbModule = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const config_1 = __webpack_require__("@nestjs/config");
const db_service_1 = __webpack_require__("../../libs/db/src/lib/db.service.ts");
let DbModule = class DbModule {
};
DbModule = tslib_1.__decorate([
    (0, common_1.Module)({
        imports: [config_1.ConfigModule],
        controllers: [],
        providers: [db_service_1.DatabaseService, config_1.ConfigService],
        exports: [db_service_1.DatabaseService],
    })
], DbModule);
exports.DbModule = DbModule;


/***/ }),

/***/ "../../libs/db/src/lib/db.service.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.DatabaseService = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const config_1 = __webpack_require__("@nestjs/config");
const client_1 = __webpack_require__("@prisma/client");
let DatabaseService = class DatabaseService extends client_1.PrismaClient {
    constructor(configService) {
        super({
            datasources: {
                db: {
                    url: configService.get('DATABASE_URL')
                }
            },
        });
        this.configService = configService;
    }
    onModuleInit() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            // await this.$connect();
        });
    }
    enableShutdownHooks(app) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.$on('beforeExit', () => tslib_1.__awaiter(this, void 0, void 0, function* () {
                yield app.close();
            }));
        });
    }
};
DatabaseService = tslib_1.__decorate([
    (0, common_1.Injectable)(),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof config_1.ConfigService !== "undefined" && config_1.ConfigService) === "function" ? _a : Object])
], DatabaseService);
exports.DatabaseService = DatabaseService;


/***/ }),

/***/ "../../libs/dto/src/index.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
const tslib_1 = __webpack_require__("tslib");
tslib_1.__exportStar(__webpack_require__("../../libs/dto/src/lib/product/category.dto.ts"), exports);
tslib_1.__exportStar(__webpack_require__("../../libs/dto/src/lib/product/picture.dto.ts"), exports);
tslib_1.__exportStar(__webpack_require__("../../libs/dto/src/lib/product/picture.input.dto.ts"), exports);
tslib_1.__exportStar(__webpack_require__("../../libs/dto/src/lib/product/product.dto.ts"), exports);
tslib_1.__exportStar(__webpack_require__("../../libs/dto/src/lib/product/product.input.dto.ts"), exports);


/***/ }),

/***/ "../../libs/dto/src/lib/product/category.dto.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.CategoryDto = void 0;
const tslib_1 = __webpack_require__("tslib");
const swagger_1 = __webpack_require__("@nestjs/swagger");
class CategoryDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], CategoryDto.prototype, "id", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], CategoryDto.prototype, "name", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], CategoryDto.prototype, "description", void 0);
exports.CategoryDto = CategoryDto;


/***/ }),

/***/ "../../libs/dto/src/lib/product/picture.dto.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.PictureDto = void 0;
const tslib_1 = __webpack_require__("tslib");
const swagger_1 = __webpack_require__("@nestjs/swagger");
class PictureDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], PictureDto.prototype, "id", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], PictureDto.prototype, "url", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], PictureDto.prototype, "productId", void 0);
exports.PictureDto = PictureDto;


/***/ }),

/***/ "../../libs/dto/src/lib/product/picture.input.dto.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.PictureInputDto = void 0;
const tslib_1 = __webpack_require__("tslib");
const swagger_1 = __webpack_require__("@nestjs/swagger");
const product_dto_1 = __webpack_require__("../../libs/dto/src/lib/product/product.dto.ts");
class PictureInputDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], PictureInputDto.prototype, "id", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], PictureInputDto.prototype, "url", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], PictureInputDto.prototype, "productId", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", typeof (_a = typeof product_dto_1.ProductDto !== "undefined" && product_dto_1.ProductDto) === "function" ? _a : Object)
], PictureInputDto.prototype, "product", void 0);
exports.PictureInputDto = PictureInputDto;


/***/ }),

/***/ "../../libs/dto/src/lib/product/product.dto.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a, _b;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ProductDto = void 0;
const tslib_1 = __webpack_require__("tslib");
const swagger_1 = __webpack_require__("@nestjs/swagger");
class ProductDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], ProductDto.prototype, "id", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], ProductDto.prototype, "name", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], ProductDto.prototype, "description", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], ProductDto.prototype, "categoryId", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", Array)
], ProductDto.prototype, "pictures", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", typeof (_a = typeof Date !== "undefined" && Date) === "function" ? _a : Object)
], ProductDto.prototype, "createdAt", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", typeof (_b = typeof Date !== "undefined" && Date) === "function" ? _b : Object)
], ProductDto.prototype, "updatedAt", void 0);
exports.ProductDto = ProductDto;


/***/ }),

/***/ "../../libs/dto/src/lib/product/product.input.dto.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a, _b;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ProductInputDto = void 0;
const tslib_1 = __webpack_require__("tslib");
const swagger_1 = __webpack_require__("@nestjs/swagger");
class ProductInputDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], ProductInputDto.prototype, "id", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], ProductInputDto.prototype, "name", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], ProductInputDto.prototype, "description", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], ProductInputDto.prototype, "categoryId", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", typeof (_a = typeof Date !== "undefined" && Date) === "function" ? _a : Object)
], ProductInputDto.prototype, "createdAt", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", typeof (_b = typeof Date !== "undefined" && Date) === "function" ? _b : Object)
], ProductInputDto.prototype, "updatedAt", void 0);
exports.ProductInputDto = ProductInputDto;


/***/ }),

/***/ "@nestjs/common":
/***/ ((module) => {

module.exports = require("@nestjs/common");

/***/ }),

/***/ "@nestjs/config":
/***/ ((module) => {

module.exports = require("@nestjs/config");

/***/ }),

/***/ "@nestjs/core":
/***/ ((module) => {

module.exports = require("@nestjs/core");

/***/ }),

/***/ "@nestjs/swagger":
/***/ ((module) => {

module.exports = require("@nestjs/swagger");

/***/ }),

/***/ "@prisma/client":
/***/ ((module) => {

module.exports = require("@prisma/client");

/***/ }),

/***/ "tslib":
/***/ ((module) => {

module.exports = require("tslib");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;

/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const core_1 = __webpack_require__("@nestjs/core");
const swagger_1 = __webpack_require__("@nestjs/swagger");
const app_module_1 = __webpack_require__("./src/app/app.module.ts");
function bootstrap() {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const app = yield core_1.NestFactory.create(app_module_1.AppModule);
        const globalPrefix = 'api';
        app.setGlobalPrefix(globalPrefix);
        const config = new swagger_1.DocumentBuilder()
            .setTitle('Product')
            .setDescription('Product API')
            .setVersion('1.0')
            .build();
        const document = swagger_1.SwaggerModule.createDocument(app, config);
        swagger_1.SwaggerModule.setup(globalPrefix, app, document);
        const port = process.env.PORT || 3333;
        yield app.listen(port);
        common_1.Logger.log(`🚀 Application is running on: http://localhost:${port}/${globalPrefix}`);
        common_1.Logger.log(`🚀 Product Swagger Endpoint : http://localhost:${port}/${globalPrefix}`);
    });
}
bootstrap();

})();

var __webpack_export_target__ = exports;
for(var i in __webpack_exports__) __webpack_export_target__[i] = __webpack_exports__[i];
if(__webpack_exports__.__esModule) Object.defineProperty(__webpack_export_target__, "__esModule", { value: true });
/******/ })()
;
//# sourceMappingURL=main.js.map